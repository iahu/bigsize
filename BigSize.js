function BigSize(img) {
    this.image = img;
    this._width = img.width;
    this._height = img.height;

    var temp = document.createElement('div');
    var html = '<div class="bigsize-contentbox"><div class="bigsize-closer">&times;</div>' +
        '<div class="bigsize-bd"></div></div>';
    html = temp.innerHTML = html;
    this.contentTemp = html;
    temp = null;
}

BigSize.prototype.show = function() {
    this.naturalSize = this.util.getNaturalSize(this.image);
    this.createMask();
    this.createBigSize();
    this.bindUI();
};
BigSize.prototype.hide = function() {
    this.hideMask();
    this.hideBigSizeBox();
};
BigSize.prototype.remove = function() {
    this.removeMask();
    this.removeBigSize();

    this.util.removeEvent(this.contentBox.children[0], 'click');
};

BigSize.prototype.bindUI = function() {
    var that = this;
    this.util.addEvent(this.contentBox.children[0], 'click', function() {
        that.remove();
    });
    this.util.addEvent(window, 'keyup', function(event) {
        event = event || window.event;
        var key = event.eventKey || event.which;
        if (key === 27) {
            that.remove();
        }
    });
    this.util.addEvent(window, 'resize', function() {
        if (that.mask) {
            that.resizeMask();
            that.resizeBigSize();
        }
    });
    function scrollHandler(event) {
        event = event || window.event;
        var delta = event.wheelDeltaY || event.wheelDelta || -event.detail * 40;

        if ( event.srcElement === that.img ) {
            that.resize(delta);
            event.returnValue = false;
        }
    }
    // this.util.addEvent(window, 'mousewheel', scrollHandler);
    // this.util.addEvent(window, 'DOMMouseScroll', scrollHandler);
};

BigSize.prototype.createMask = function() {
    var mask = document.createElement('div');

    mask.className = 'bigsize-mask';
    this.mask = mask;
    this.resizeMask();
    document.body.appendChild(mask);
};
BigSize.prototype.resizeMask = function() {
    var mask = this.mask,
        getElementSize = this.util.getElementSize,
        w, h;
    if (!mask) {
        return;
    }
    mask.style.width = 0;
    mask.style.height = 0;
    w = getElementSize('width');
    h = getElementSize('height');

    mask.style.width = w + 'px';
    mask.style.height = h + 'px';

    getElementSize = null;
    mask = null;
    w = null;
    h = null;
};

BigSize.prototype.removeMask = function() {
    if (this.mask) {
        this.util.removeElement(this.mask);
        this.mask = null;
    }
};
BigSize.prototype.hideMask = function() {
    this.mask.style.display = 'none';
};
BigSize.prototype.showMask = function() {
    this.mask.style.display = 'block';
};

BigSize.prototype.createBigSize = function() {
    var bigsizeBox = document.createElement('div');
    var img = new Image();
    var contentBox, bdBox;

    bigsizeBox.className = 'bigsize';
    bigsizeBox.innerHTML = this.contentTemp;
    contentBox = bigsizeBox.children[0];
    bdBox = contentBox.children[1];

    img.src = this.image.src;
    img.className = 'bigsize-img';
    this.contentImage = img;
    this.resetImageSize(img);
    bdBox.appendChild(img);

    this.bigsizeBox = bigsizeBox;
    this.contentBox = contentBox;
    document.body.appendChild(bigsizeBox);
    this.resizeBigSize();

    bigsizeBox = null;
    img = null;
    contentBox = null;
    bdBox = null;
};
BigSize.prototype.resizeBigSize = function() {
    var contentBox = this.contentBox,
        img = this.contentImage,
        // getElementSize = this.util.getElementSize,
        pageSize = this.util.getPageSize(),
        mt = document.body.scrollTop || document.documentElement.scrollTop;
    if (!contentBox) {
        return;
    }
    var size = this.resetImageSize(img);

    mt += (pageSize.windowHeight - size.height) / 2;
    contentBox.style.marginTop = mt + 'px';
    contentBox = null;
    this.img = null;
    this.img = img;
};

BigSize.prototype.hideBigSize = function() {
    this.bigsizeBox.style.display = 'none';
};
BigSize.prototype.showBigSize = function() {
    this.bigsizeBox.style.display = 'block';
};
BigSize.prototype.removeBigSize = function() {
    this.util.removeElement(this.bigsizeBox);
    this.bigsizeBox = null;
    this.img = null;
};

BigSize.prototype.resetImageSize = function(img) {
    var naturalSize = this.naturalSize;
    var width = naturalSize.width;
    var height = naturalSize.height;
    var pageSize = this.util.getPageSize();
    var maxWidth = pageSize.windowWidth - 160;
    var maxHeight = pageSize.windowHeight - 160;
    var w,h;

    maxWidth = Math.min(800, Math.max(200, maxWidth));
    maxHeight = Math.min(600, Math.max(150, maxHeight));

    if ((width/height) > (maxWidth/maxHeight)) {
        if (width > maxWidth) {
            w = maxWidth;
            h = maxWidth * height / width;
        } else {
            w = naturalSize.width;
            h = naturalSize.height;
        }
    } else {
        if (height > maxHeight) {
            w = maxHeight * width / height;
            h = maxHeight;
        } else {
            w = naturalSize.width;
            h = naturalSize.height;
        }
    }
    img.style.width = w + 'px';
    img.style.height = h + 'px';
    return {
        width: w,
        height: h
    };
};
BigSize.prototype.resize = function(delta) {
    var img = this.img;
    var w = img.width,
        h = img.height,
        a = w/h;

    delta /= 2;
    img.style.width = w+ delta*a + 'px';
    img.style.height = h+ delta/a + 'px';
};

BigSize.prototype.util = {
    getElementSize: function(name, el) {
        name = name.slice(0, 1).toUpperCase() + name.slice(1);
        if (el) {
            return Math.max(
                el['scroll' + name],
                el['offset' + name],
                el['client' + name]
            );
        } else {
            return Math.max(
                document.body['scroll' + name], document.documentElement['scroll' + name],
                document.body['offset' + name], document.documentElement['offset' + name],
                document.body['client' + name], document.documentElement['client' + name]
            );
        }
    },
    getNaturalSize: function(img) {
        var size = {};
        if (typeof img.naturalWidth !== 'undefined') {
            // HTML5 browsers
            size.width = img.naturalWidth;
            size.height = img.naturalHeight;
        } else {
            // IE 6/7/8
            var i = new Image();
            i.src = img.src;
            size.width = i.width;
            size.height = i.height;
        }
        return size;
    },
    addClass: function(el, cls) {
        el.className += ' ' + cls;
    },
    removeClass: function(el, cls) {
        var c = el.className.replace(cls, '');
        el.className = c;
    },
    removeElement: function(el) {
        if (el) el.parentElement.removeChild(el);
    },
    addEvent: function(el, evt, fn) {
        if (el.addEventListener) {
            el.addEventListener(evt, fn);
        } else if (el.attachEvent) {
            el.attachEvent('on' + evt, fn);
        } else {
            el['on' + evt] = fn;
        }
    },
    removeEvent: function(el, evt, fn) {
        if (el.removeEventListener) {
            el.removeEventListener(evt, fn);
        } else if (el.dispatchEvent) {
            el.dispatchEvent('on' + evt, fn);
        } else {
            el['on' + evt] = null;
        }
    },
    getPageSize: function(){

        var xScroll, yScroll,pageHeight,pageWidth;
        var self = this;
        if (window.innerHeight && window.scrollMaxY) {
            xScroll = document.body.scrollWidth;
            yScroll = window.innerHeight + window.scrollMaxY;
        } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
            xScroll = document.body.scrollWidth;
            yScroll = document.body.scrollHeight;
        } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
            xScroll = document.body.offsetWidth;
            yScroll = document.body.offsetHeight;
        }

        var windowWidth, windowHeight;
        if (self.innerHeight) { // all except Explorer
            windowWidth = self.innerWidth;
            windowHeight = self.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
            windowWidth = document.documentElement.clientWidth;
            windowHeight = document.documentElement.clientHeight;
        } else if (document.body) { // other Explorers
            windowWidth = document.body.clientWidth;
            windowHeight = document.body.clientHeight;
        }

        // for small pages with total height less then height of the viewport
        if(yScroll < windowHeight){
            pageHeight = windowHeight;
        } else {
            pageHeight = yScroll;
        }

        // for small pages with total width less then width of the viewport
        if(xScroll < windowWidth){
            pageWidth = windowWidth;
        } else {
            pageWidth = xScroll;
        }

        return {
            pageWidth: pageWidth,
            pageHeight: pageHeight,
            windowWidth: windowWidth,
            windowHeight: windowHeight
        };
    }
};

if (typeof module !== 'undefined') {
    module.exports = BigSize;
}